from django import forms
from allauth.account.forms import SignupForm, LoginForm
from allauth.account.adapter import get_adapter
import models
from django.utils.translation import pgettext, ugettext_lazy as _, ugettext

class SignupComplete(forms.ModelForm,SignupForm):
	class Meta:
		model=models.User
		fields = ('first_name','last_name','telefono','telefono2')
	
	def clean_password2(self):
		if 'password1' in self.cleaned_data and 'password2' in self.cleaned_data:
			if self.cleaned_data['password1']!=self.cleaned_data['password2']:
			
				raise forms.ValidationError(_("You must type the same password"
											" each time."))
			else:
				del self.cleaned_data['password2']
		return self.cleaned_data

	def save(self, request):
		if True:
			adapter = get_adapter()
			user = adapter.new_user(request)
			#print request
			user_save = adapter.save_user(request, user, self)
			user_save.first_name=request.POST['first_name']
			user_save.last_name=request.POST['last_name']	
			user_save.phone=request.POST['phone']
			user_save.save()
			self.custom_signup(request, user)
			#self.setup_user_email(request, user, [])
		#except Exception, e:
		#	raise forms.ValidationError("Ocurrio un error inesperado por favor vuelva a intentar la operacion.")	
		return user

class SignupPublication(forms.ModelForm):
	class Meta:
		model=models.UserPublication
		fields = ('first_name','telefono','telefono2','email','password')



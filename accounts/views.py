# -*- encoding:utf-8 -*-
from django.shortcuts import render
from allauth.account.views import  LoginView, SignupView
from django.core.urlresolvers import reverse
from django.views.generic import UpdateView
from accounts.models import User
from django.contrib.auth.hashers import make_password
import form
# Create your views here.
class Login(LoginView):
	template_name="accounts/login.html"
	
	def get_context_data(self, **kwargs):
		context=super(Login,self).get_context_data(**kwargs)
		context['title']="Iniciar Sesión"
		return context

	def get_succes_url(self):
		return reverse('home')


class SignUp(SignupView):
	template_name="accounts/signup.html"
	form_class=form.SignupComplete
	
	def get_context_data(self, **kwargs):
		context=super(SignUp,self).get_context_data(**kwargs)
		context['title']="Registro de Usuario"
		return context
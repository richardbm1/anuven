from django.contrib import admin
from models import User, UserPublication
# Register your models here.

admin.site.register(User)
admin.site.register(UserPublication)
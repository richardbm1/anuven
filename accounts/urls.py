from django.conf.urls import include, url
from accounts import views as account
from django.views.generic.base  import RedirectView
from django.contrib.auth.decorators import login_required

urlpatterns = [
    #url(r'^(?P<slug>[\w-]+)/$',panel.Templates.as_view(),name='templates'),
    url(r'^login/$',account.Login.as_view(),name='login'),
    url(r'^signup/$',account.SignUp.as_view(),name='signup'),
    #url(r'^perfil/(?P<pk>[\w-]+)/$',login_required(account.ProfileView.as_view()),name='perfil'),
    #url(r'profile/$', login_required(RedirectView.as_view(url="/", permanent=False)), name='profile'),


]

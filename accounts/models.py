# -*- encoding:utf-8 -*-
from __future__ import unicode_literals
from django.template import defaultfilters
from django.core.urlresolvers import reverse
from django.db import models
from django.contrib.auth.models import AbstractUser, AbstractBaseUser
# Create your models here.


class User(AbstractUser):
	telefono=models.CharField(max_length=30)
	telefono2=models.CharField(max_length=30,blank=True,null=True)
	slug = models.SlugField(max_length=100,blank=True,null=True)

	def __unicode__(self):
		return self.email

	def save(self, *args, **kwargs):
		self.slug = self.id
		super(User,self).save(*args,**kwargs)

	def get_absolute_url(self):
		return reverse('home')

	class Meta:
		verbose_name = 'Usuario'
		verbose_name_plural = 'Usuarios'


class UserPublication(AbstractBaseUser):
	first_name = models.CharField(max_length=50)
	email = models.EmailField()
	telefono=models.CharField(max_length=30)
	telefono2=models.CharField(max_length=30,blank=True,null=True)
	slug=models.SlugField(max_length=100,blank=True,null=True)
	
	def __unicode__(self):
		return self.email

	def save(self, *args, **kwargs):
		self.slug = defaultfilters.slugify(self.id)
		super(UserPublication,self).save(*args,**kwargs)

	def get_absolute_url(self):
		return reverse('home')

	class Meta:
		verbose_name = 'Usuario de Publicación'
		verbose_name_plural = 'Usuarios de Publicación'
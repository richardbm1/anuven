from django.core.serializers import serialize
from django.core.serializers.json import DjangoJSONEncoder
from django.db.models.query import QuerySet
try:
    from django.utils import simplejson as json
except ImportError:
    import json
from django.utils.safestring import mark_safe
from django.template import Library

register = Library()
from django.forms.models import model_to_dict

@register.filter
def jsonify(object):
    if isinstance(object, QuerySet):
        return mark_safe(serialize('json', object))
    return mark_safe(simplejson.dumps(object))


@register.filter
def pricecomma(number):
	return "{:,.2f}".format(number)

@register.filter
def numbercontact(number):
	if len(number)==10:
		return "{0} {1}-{2}".format(number[0:2],number[2:6],number[6:10])
	return number

@register.filter
def salto(arg):
	return arg.split('\r')
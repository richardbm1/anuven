$(document).ready(function(){
$('#ultimasP').html(ultimasPub(10));
$('#pDestacadas').html(pDestacadas(10));
$('#inputBuscar').on('click',function(){
		titulo=$('#buscarTitulo').val().split(" ").join('-');
		
		cat=$('#buscarCat').val().split(" ").join('-');
		ubicacion=$('#buscarEstado').val().split(" ").join('-');

		var url = "/listado/"+ubicacion+"/"+cat+"/"+titulo; 
		$(location).attr('href',url);


	});
});

	function ultimasPub(cantidad){

		
		$.ajax({
				data:{'cantidad':cantidad},
				url: /ultimasPublicaciones/,
				type : 'get',
					success : function(data){
						console.log(data)
						html = "<h2>Ultimas Publicaciones</h2>";
						for(var i = 0; i<data.length ; i++){

							console.log(data[i])
							html += '<div class="col-xs-12 col-md-12"><div class="media"><a href="/anuncio/'
									+data[i].slug+'" class="pull-left"><img src="'+data[i].imagen_principal
									+'" %}" class="media-object img-responsive" alt="imagen" width="150"></a>'
									+'<div class="media-body pic-publication"><a href="/anuncio/'+data[i].slug
									+'"><h4 class="media-heading"><b>'+data[i].titulo+'</b></h4></a>'
							     	+'<small><i>'+data[i].estado+'</i></small>'
							     	+'<p class="no-bottom precio"><b><span>$'+data[i].precio
							     	+'</span></b></p></div></div><hr></div>';
							$('#ultimasP').html(html);

						}

						

						
					}


			});
	}
		function pDestacadas(cantidad){

		
		$.ajax({
				data:{'cantidad':cantidad},
				url: /publicacionesDestacadas/,
				type : 'get',
					success : function(data){
						console.log(data)
						html = "";
						for(var i = 0; i<data.length ; i++){

							console.log(data[i])
							html += '<div class="media"><a href="/anuncio/'
									+data[i].slug+'" class="pull-left"><img src="'+data[i].imagen_principal
									+'" %}" class="media-object img-responsive" alt="imagen" width="90">'
									+'</a><h5 class="media-heading"><b><a href="/anuncio/'+data[i].slug+'">'
									+data[i].titulo+'</a></b> <small><i>'+data[i].estado+'</i></small></h5><P class="descripcion">'
									+data[i].descripcion+'</P><p class="no-bottom precio-2"><b><span>$'+data[i].precio
									+'</span></b></p></div></div><hr>';
							$('#pDestacadas').html(html);

						}

						
								
						
					}


			});

		
	}

	var campos_cat = "";

	$(document).ready(function(){

	
		
		if(typeof estado!="undefined" && estado!=null){
			$('#terminos').hide();
		}
		console.log(Cat);
		if(Cat!=null){
			SelectCat(Cat,id_Cat);
		}
		/* listado */
		$('#subcat a:gt(10)').hide();
		$('#subcat').css('height','16em');

		$('#verMasSubCat').on('click',function(){
			$('#subcat a:gt(10)').show();
			$('#subcat').css('height','');
			$('#verMasSubCat').hide();
			
		});

		$('#divEstados a:gt(10)').hide();
		$('#divEstados').css('height','16em');

		$('#verMasEstados').on('click',function(){
			$('#divEstados a:gt(10)').show();
			$('#divEstados').css('height','');
			$('#verMasEstados').hide();
			
		});
		$('#inputBuscar').on('click',function(){
		
		var url="/anuncios";
		
		ubicacion="/";
		if ($('#buscarEstado').val()!=""){
			ubicacion+=$('#buscarEstado').val().split(" ").join('-');
			url+=ubicacion;
		}
		cat="/";
		if ($('#id_categoria').val()!=""){
			cat+=$('#id_categoria').text().split(" ").join('-');
			url+=cat;
		}else{
			cat+="todas-las-categorias";
			url+=cat;
		}
		titulo="/";
		if ($('#buscarTitulo').val()!=""){
			titulo+=$('#buscarTitulo').val().split(" ").join('-');
			url+=titulo;
		}
		
		
		$('#id_buscar').attr('action',url).submit();
		

	});
		/* Detalles de publicacion*/
		$('#pDestacadas').html(pDestacadas(10));
		$('#botoncontact').on('click',function(){
			if(!$('#infocontact').is(':visible')){
				$('#infocontact').fadeIn();
			}else{
				$('#infocontact').fadeOut();
			}
			
		});
		$('#botoneditar').on('click',function(){

			$('#logineditar').fadeIn();
		});
		$('#botonmessage').on('click',function(){
			if(!$('#messagecontact').is(':visible')){
				$('#messagesuccess').fadeOut();
				$('#messagecontact').fadeIn();
			}else{
				$('#messagecontact').fadeOut();
			}
			
		});


		
		if(typeof Cat!="undefined" && Cat!=null){
			SelectCat(Cat,id_Cat);
		}
		if(update==true){
			$('#file-es').fileinput({
		        language: 'es',
		        uploadUrl: '/publicar_images/',
		        allowedFileExtensions : ['jpg', 'png','gif','jpeg'],
		        uploadExtraData:{
		        csrfmiddlewaretoken: csrftoken,
		        publicacion:id_publicacion,
		        image:$('#file-es').val(),
		        },
		        maxFileCount: 5,
		        showUpload:true,
		        initialPreview: imagenes,
				initialPreviewConfig: imagenes_config
		        
		    });
		    
		    $("#file-0").fileinput({
		        'allowedFileExtensions' : ['jpg', 'png','gif','jpeg'],
		    });

		    $('#file-es').on('filepredelete', function(event, key) {
			});
		}else{
			$('#file-es').fileinput({
		        language: 'es',
		        allowedFileExtensions : ['jpg', 'png','gif','jpeg'],
		        
		        maxFileCount: 5,
		        showUpload:false
		        
		    });
		    
		    $("#file-0").fileinput({
		        'allowedFileExtensions' : ['jpg', 'png','gif','jpeg'],
		    });
		}
		

	    $('#id_estado').ready(function(){
	    	$.ajax({
	    		url : /estado/,
					type : 'get',
					success : function(data){
						var html = "<option> </option>"
						
						for(var i = 0; i<data.length ; i++){
							var selected="";
							if(estado!=null && estado==data[i].pk){
								selected="selected";
							}
							html += '<option value="'+data[i].pk+'" '+selected+'>'+data[i].fields.estado+'</option>'
						}
						$('#id_estado').html(html);
						if(estado!=null){
							ciudades_auto(estado);

						}
					}
	    	});
	    });
	    function ciudades_auto(estado){
	    	var id = estado;
			if (id!=""){
				$.ajax({
					data : {'id':id},
					url : /ciudad/,
					type : 'get',
					success : function(data){
						var html = "<option value=''> </option>"
						for(var i = 0; i<data.length ; i++){
							var selected="";
							if(ciudad!=null && ciudad==data[i].pk){
								selected="selected";
							}
							html += '<option value="'+data[i].pk+'" '+selected+'>'+data[i].fields.ciudad+'</option>'
						}
						$('#id_ciudad').html(html);
					}

				});
			}
			
	    }

		$('#id_Marca_AyC').on('change',modelos_AyC);
		$('#id_estado').on('change',function(){
			var id = $(this).val();
			if (id!=""){
				$.ajax({
					data : {'id':id},
					url : /ciudad/,
					type : 'get',
					success : function(data){
						var html = ""
						for(var i = 0; i<data.length ; i++){
							html += '<option value="'+data[i].pk+'">'+data[i].fields.ciudad+'</option>'
						}
						$('#id_ciudad').html(html);
					}

				});
			
			}else{

				var html = ""
				html += '<option value=""></option>'
				$('#id_ciudad').html(html);
			}
			});


		$('#id_categoria').on('change', function(){

			var categoria = $('#id_categoria');
			if(categoria.val() == ""){
				$('#error_categoria').fadeIn();
			}else{
				$('#error_categoria').fadeOut();
				
				ocultar_cat();
				mostrar_cat(categoria.val());
			}
			
		});
		$('#particular_o_negocio').on('change',function(){
			var rad=$('#particular_o_negocio:checked').val();
			if(rad == ""){
				$('#error_pot').fadeIn();
			}else{
				$('#error_pot').fadeOut();
			}
		});
		$('#condicion').on('change',function(){
			var cond=$('#condicion:checked').val();
			if(cond == ""){
				$('#error_condicion').fadeIn();
			}else{
				$('#error_condicion').fadeOut();
			}
		});
		$('#tipo').on('change',function(){
			var tipo=$('#tipo:checked').val();
			if(tipo == ""){
				$('#error_tipo').fadeIn();
			}else{
				$('#error_tipo').fadeOut();
			}
		});


		$('#id_titulo').on('change',function(){
			var titulo = $('#id_titulo').val();
			if(titulo == ""){
				$('#error_titulo').fadeIn();
			}else{
				$('#error_titulo').fadeOut();
			}

		});
		$('#id_Cas_terreno').on('change',function(){
			var terreno = $('#id_Cas_terreno').val();
			if(terreno == ""){
				$('#error_Cas_terreno').fadeIn();
			}else{
				$('#error_Cas_terreno').fadeOut();
			}
		});
		$('#id_Cas_construccion').on('change',function(){
			var construccion = $('#id_Cas_construccion').val();
			if(construccion == ""){
				$('#error_Cas_construccion').fadeIn();
			}else{
				$('#error_Cas_construccion').fadeOut();
			}

		});
		$('#id_comentario').on('change',function(){
			var titulo = $('#id_comentario').val();
			if(titulo == ""){
				$('#error_comentario').fadeIn();
			}else{
				$('#error_comentario').fadeOut();
			}

		});

		$('#id_precio').on('change',function(){
			numero=/^\d+(?:[\.,]?\d{1,2})?$/
			if ($('#id_precio').val()==""){
				$('#error_precio').fadeIn();
			}else{
				$('#error_precio').fadeOut();
				if(!numero.test($('#id_precio').val())){
					$('#error_precio').fadeIn();
				}else{
					$('#error_precio').fadeOut();
				}
				
			}

		});


		$('#id_estado').on('change',function(){	
			if ($('#id_estado').val()==""){
				$('#error_estado').fadeIn();
			}else{
				$('#error_estado').fadeOut();
			}
		});
		$('#id_ciudad').on('change',function(){	
			if ($('#id_ciudad').val()==""){
				$('#error_ciudad').fadeIn();
			}else{
				$('#error_ciudad').fadeOut();
			}
		});


		$('#id_mail').on('change',function(){
			var val_email = /[a-z0-9!#$%&'*+/=?^_`{|}~-][a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9]{2}(?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9]{2}(?:[a-z0-9-]+[a-z0-9])?/;
			var correo = $('#id_mail').val();
			if(correo == ""){
				$('#error_mail').fadeIn();
				$('#error_mail2').fadeOut();
			}else{
				$('#error_mail').fadeOut();
			
				if(!val_email.test(correo)){
					$('#error_mail2').fadeIn();
				}else{
					$('#error_mail2').fadeOut();
					usermail=$('#id_mail').val().replace(/@/g,'')
					usermail=usermail.replace(/\./g,'')
					$('#id_username').val(usermail);
				}
			}
		});

		$('#id_Marca_AyC').on('change',function(){
			var marca = $('#id_Marca_AyC');
			if(marca.val() == ""){
				$('#error_marca_AyC').fadeIn();
			}else{
				$('#error_marca_AyC').fadeOut();
			}				
		});	
		$('#id_Modelo_AyC').on('change',function(){
			var modelo = $('#id_Modelo_AyC');
			if(modelo.val() == ""){
				$('#error_modelo_AyC').fadeIn();
			}else{
				$('#error_modelo_AyC').fadeOut();
			}				
		});	
		$('#anio').html(function(){
			var html="<option></option";
			
				
			for(i=2016;i>=1920;i--){
				var selected="";
				if(anio!=null){
					selected="selected";
				}

				html+="<option value='"+i+"' "+selected+">"+i+"</option>";
			}
			return html
		});		
		$('#kilometraje_AyC').on('change',function(){
			
			if ($('#kilometraje_AyC').val()==""){
				$('#error_kilometraje_AyC').fadeIn();
			}else{
				$('#error_kilometraje_AyC').fadeOut();
			}
		});	
		$('#id_nombre').on('change',function(){
			
			if ($('#id_nombre').val()==""){
				$('#id_error_nombre').fadeIn();
			}else{
				$('#id_error_nombre').fadeOut();
			}
		});		
		$('#id_telefono').on('change',function(){
			
			if ($('#id_telefono').val()==""){
				$('#id_error_telefono').fadeIn();
			}else{
				$('#id_error_telefono').fadeOut();
			}
		});		
		$('#id_password').on('change',function(){
			var pass = /^[A-Za-z0-9_-]{5}[A-Za-z0-9_-]+$/;
			if ($('#id_password').val()==""){
				$('#id_error_password2').fadeOut();
				$('#id_error_password').fadeIn();
			}else{				
				$('#id_error_password').fadeOut();
				
				if(!pass.test($('#id_password').val())){
					$('#id_error_password2').fadeIn();
				}else{
					$('#id_error_password2').fadeOut();
				}
			}
		});		
		$('#sendmessage').click(function(){	
			submit=true;
			if ($('#id_nombre').val()==""){
				$('#id_error_nombre').fadeIn();
				
				submit = false;
			}else{
				$('#id_error_nombre').fadeOut();
			}
			
			if($('#id_mail').val() == ""){
				$('#error_mail').fadeIn();
				$('#error_mail2').fadeOut();
				
				submit = false;
			}else{
				$('#error_mail').fadeOut();
				var val_email = /[a-z0-9!#$%&'*+/=?^_`{|}~-][a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9]{2}(?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9]{2}(?:[a-z0-9-]+[a-z0-9])?/;
			
				if(!val_email.test($('#id_mail').val())){
					$('#error_mail2').fadeIn();
					
					submit = false;
				}else{
					$('#error_mail2').fadeOut();
					
				}
			}
			if ($('#id_comentario').val()==""){
					$('#error_comentario').fadeIn();
					
					submit = false;
				}else{
					$('#error_comentario').fadeOut();
				}
			if (submit==false){
				return false;
			}else{
				$.post("/send-message/",
					{
						csrfmiddlewaretoken: csrftoken,
						nombre: $('#id_nombre').val(),
						telefono: $('#id_telefono').val(),
						email: $('#id_mail').val(),
						comentario: $('#id_comentario').val(),
						email_vendedor: email_vendedor,
						titulo: titulo

					},
					function(data, status){
						$('#messagecontact').fadeOut();
						$('#messagesuccess').fadeIn();
						
				});
				
			}
		});
		$('#publish').click(function(){
			var titulo = $('#id_titulo').val();
			var categoria = $('#id_categoria');
			var correo = $('#id_mail').val();
			var radio = $('input[type = "radio"]:checked');
			var cond = $('input[type = "checkbox"]:checked');
			submit = true;

			if(titulo == ""){
				$('#error_titulo').fadeIn();
				
				submit = false;
			}else{
				$('#error_titulo').fadeOut();
				

			}
			if($('#particular_o_negocio:checked').length ==0){

				$('#error_pot').fadeIn();
				
				submit=false;
			}else{
				$('#error_pot').fadeOut();
			}

			if($('#condicion:checked').length ==0){

				$('#error_condicion').fadeIn();
				
				submit=false;
			}else{
				$('#error_condicion').fadeOut();
			}
			if($('#tipo:checked').length ==0){

				$('#error_tipo').fadeIn();
				
				submit=false;
			}else{
				$('#error_tipo').fadeOut();
			}
			if ($('#id_comentario').val()==""){
					$('#error_comentario').fadeIn();
					
					submit = false;
				}else{
					$('#error_comentario').fadeOut();
				}
			if ($('#id_precio').val()==""){
				$('#error_precio').fadeIn();
				
				submit = false;
			}else{
				numero=/^\d+(?:[\.,]\d+)?$/
				if(!numero.test($('#id_precio').val())){
					$('#error_precio').fadeIn();
				}else{
					$('#error_precio').fadeOut();
				}
				$('#error_precio').fadeOut();
			}
			if ($('#id_estado').val()==""){
				$('#error_estado').fadeIn();
				
				submit = false;
			}else{
				$('#error_estado').fadeOut();
			}
			if ($('#id_ciudad').val()==""){
				$('#error_ciudad').fadeIn();
				
				submit = false;
			}else{
				$('#error_ciudad').fadeOut();
			}

			if($('#id_mail').val() == ""){
				$('#error_mail').fadeIn();
				$('#error_mail2').fadeOut();
				
				submit = false;
			}else{
				$('#error_mail').fadeOut();
				var val_email = /[a-z0-9!#$%&'*+/=?^_`{|}~-][a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9]{2}(?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9]{2}(?:[a-z0-9-]+[a-z0-9])?/;
			
				if(!val_email.test($('#id_mail').val())){
					$('#error_mail2').fadeIn();
					
					submit = false;
				}else{
					$('#error_mail2').fadeOut();
					
				}
			}
			if ($('#id_nombre').val()==""){
				$('#id_error_nombre').fadeIn();
				
				submit = false;
			}else{
				$('#id_error_nombre').fadeOut();
			}
			if ($('#id_telefono').val()==""){
				$('#id_error_telefono').fadeIn();
				
				submit = false;
			}else{
				$('#id_error_telefono').fadeOut();
			}
			var pass = /^[A-Za-z0-9_-]{5}[A-Za-z0-9_-]+$/;
			if ($('#id_password').val()==""){
				$('#id_error_password2').fadeOut();
				$('#id_error_password').fadeIn();
				
				submit = false;
			}else{				
				$('#id_error_password').fadeOut();
				
				if(!pass.test($('#id_password').val())){
					$('#id_error_password2').fadeIn();
					
					submit = false;
				}else{
					$('#id_error_password2').fadeOut();
				}
			}

			if(radio.length == 0){
				$('#error_radio').fadeIn();
				
				submit = false;
			}else{
				$('#error_radio').fadeOut();
				
			}
			if(typeof estado=="undefined" || estado==null){

				if(!$('#id_condiciones').prop('checked')){
					$('#error_terminos').fadeIn();
					
					submit = false;
				}else{
					$('#error_terminos').fadeOut();
					
				}
			}
		
			if(categoria.val() == ""){
				$('#error_categoria').fadeIn();
				
				submit = false;
			}else{
				$('#error_categoria').fadeOut();
				
			}

			switch(campos_cat){
				case 'AyC':
				var marca = $('#id_Marca_AyC');
				if(marca.val() == ""){
					$('#error_marca_AyC').fadeIn();
					
					submit = false;
				}else{
					$('#error_marca_AyC').fadeOut();
					
				}
				var marca = $('#id_Marca_AyC');
				if(marca.val() == ""){
					$('#error_marca_AyC').fadeIn();
					
					submit = false;
				}else{
					$('#error_marca_AyC').fadeOut();
				}			
				if ($('#kilometraje_AyC').val()==""){
					$('#error_kilometraje_AyC').fadeIn();
					
					submit = false;
				}else{
					$('#error_kilometraje_AyC').fadeOut();
				}
				break;

				case 'Cas':
				var terreno = $('#id_Cas_terreno').val();
				if(terreno == ""){
					$('#error_Cas_terreno').fadeIn();
					
					submit=false;
				}else{
					$('#error_Cas_terreno').fadeOut();
				}
				var construccion = $('#id_Cas_construccion').val();
				if(construccion == ""){
					$('#error_Cas_construccion').fadeIn();
				}else{
					$('#error_Cas_construccion').fadeOut();
				}
				break;
			}

			if (submit==false){
				return false;
			}else{
				$('#formenv').submit();
			}

		});


	});

		function mostrar_cat(cat){
			switch(cat){
				case 'AyC':
					$('#id_AyC').fadeIn();
					$.ajax({
						url:/marcas/,
						type:'get',
						success:function(data){
							var html = "<option> <option>"
							
							for(var i = 0; i<data.length ; i++){
								var selected="";
								if(marca!=null && marca==data[i].pk){
									selected="selected";
								}
								html += '<option value="'+data[i].pk+'" '+selected+'>'+data[i].fields.marca+'</option>';
							}
							$('#id_Marca_AyC').html(html);
						}
						

					});
					if(marca!=null){
						modelos_AyC_auto(marca);
					}
					if(transmision!=null){
						$('#transmision option[value='+transmision+']').attr("selected","selected");
					}
					if(combustible!=null){
						$('#combustible option[value='+combustible+']').attr("selected","selected");
					}
					if(kilometraje!=null){
						$('#kilometraje_AyC').attr('value',kilometraje);
					}
					campos_cat=cat;
					break;
				
				case 'Cas':
					$('#id_Cas').fadeIn();
					campos_cat=cat;
					break;
				}
		}

		function ocultar_cat(){
			$('#id_AyC').fadeOut();
			$('#id_Cas').fadeOut();

		}

		function modelos_AyC(){
			var id = $(this).val();
			if (id!=""){
				$.ajax({
					data : {'id':id},
					url : /modelos/,
					type : 'get',
					success : function(data){
						var html = "<option> </option>"
						for(var i = 0; i<data.length ; i++){
							html += '<option value="'+data[i].pk+'">'+data[i].fields.modelo+'</option>'
						}
						$('#id_Modelo_AyC').html(html);
					}

				});
			}else{
				var html = ""
				html += '<option value="">Seleccione</option>'
				$('#id_Modelo_AyC').html(html);
			}
		}
		function modelos_AyC_auto(id_marca){
			var id = id_marca;
			if (id!=""){
				$.ajax({
					data : {'id':id},
					url : /modelos/,
					type : 'get',
					success : function(data){
						var html = ""
						for(var i = 0; i<data.length ; i++){
							var selected="";
								if(modelo!=null && modelo==data[i].pk){
									selected="selected";
								}
							html += '<option value="'+data[i].pk+'"'+selected+'>'+data[i].fields.modelo+'</option>'
						}
						$('#id_Modelo_AyC').html(html);
					}

				});
			}else{
				var html = ""
				html += '<option value="">Seleccione</option>'
				$('#id_Modelo_AyC').html(html);
			}
		}
	
		

	function SelectCat(cat,id){
		if(cat==""){
			cat="<b>categoria</b>"
		}
		$('#id_categoria').html('<option value="'+id+'" selected>'+cat+'</option>');
		$('#button_cat').html(cat+'<i class="fa fa-chevron-down right"></i>');
		ocultar_cat();
		mostrar_cat(id);
	}

	function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

function slug(str) {
    var $slug = '';
    var trimmed = $.trim(str);
    $slug = trimmed.replace(/[^a-z0-9-]/gi, '-').
    replace(/-+/g, '-').
    replace(/^-|-$/g, '');
    return $slug
}
	
function pDestacadas(cantidad){

		
		$.ajax({
				data:{'cantidad':cantidad},
				url: /publicacionesDestacadas/,
				type : 'get',
					success : function(data){
						html = "";
						for(var i = 0; i<data.length ; i++){

							html += '<div class="media"><a href="/anuncio/'
									+data[i].slug+'" class="pull-left"><img src="'+data[i].imagen_principal
									+'" %}" class="media-object img-responsive" alt="imagen" width="90">'
									+'</a><h5 class="media-heading"><b><a href="/anuncio/'+data[i].slug+'">'
									+data[i].titulo+'</a></b> <small><i>'+data[i].estado+'</i></small></h5><P class="descripcion">'
									+data[i].descripcion+'</P><p class="no-bottom precio-2"><b><span>$'+data[i].precio
									+'</span></b></p></div></div><hr>';
							$('#pDestacadas').html(html);

						}

						
								
						
					}


			});

		
	}
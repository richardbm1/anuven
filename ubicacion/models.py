from __future__ import unicode_literals

from django.db import models

# Create your models here.

class Estado(models.Model):
	estado=models.CharField(max_length=50)

	def __unicode__(self):
		return self.estado

class Ciudad(models.Model):
	estado=models.ForeignKey(Estado)
	ciudad=models.CharField(max_length=50)

	class Meta:
		verbose_name_plural = "Ciudades"

	def __unicode__(self):
		return self.ciudad
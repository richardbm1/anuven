from __future__ import unicode_literals

from django.db import models
from rest_framework.authtoken.models import Token
from accounts.models import UserPublication
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
import binascii
import os
# Create your models here.


class Token2(models.Model):
    """
    The default authorization token model.
    """
    key = models.CharField(_("Key"), max_length=40, primary_key=True)
    user = models.OneToOneField(UserPublication, related_name='auth_token',
                                on_delete=models.CASCADE, verbose_name=_("User2"))
    created = models.DateTimeField(_("Created"), auto_now_add=True)

    class Meta:
        # Work around for a bug in Django:
        # https://code.djangoproject.com/ticket/19422
        #
        # Also see corresponding ticket:
        # https://github.com/tomchristie/django-rest-framework/issues/705
        abstract = 'rest_framework.authtoken' not in settings.INSTALLED_APPS
        verbose_name = _("Token2")
        verbose_name_plural = _("Tokens2")

    def save(self, *args, **kwargs):
        if not self.key:
            self.key = self.generate_key()
        return super(Token2, self).save(*args, **kwargs)

    def generate_key(self):
        return binascii.hexlify(os.urandom(20)).decode()

    def __str__(self):
        return self.key

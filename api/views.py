from django.shortcuts import render
from api.serializers import DetallesSerializer, AuthTokenSerializer2, UserSerializer
from rest_framework import viewsets, filters, views, parsers, generics, mixins
from rest_framework.response import Response
from publications.models import Publicacion, Images
from rest_framework.authtoken.views import ObtainAuthToken
from accounts.models import User
from api.models import Token2
from api import permissions
from rest_framework import status
from rest_framework.permissions import AllowAny
from ubicacion.models import Estado, Ciudad
import json
# Create your views here.


class Register(viewsets.ModelViewSet):
	serializer_class=UserSerializer
	permission_classes=(permissions.SI,)
	queryset=User.objects.all()
	lookup_field='id'
	def create(self, request, *args, **kwargs):
		serializer = self.get_serializer(data=request.data)
		serializer.is_valid(raise_exception=True)
		self.perform_create(serializer)
 		headers = self.get_success_headers(serializer.data)
 		return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

class DetallesView(viewsets.ModelViewSet):
	permission_classes=(permissions.IsOwner, )
	queryset=Publicacion.objects.all()
	serializer_class = DetallesSerializer
	lookup_field='slug'
	filter_backends = (filters.DjangoFilterBackend,)
	filter_fields = ('precio', 'categoria','tipo','particular_o_negocio','estado','condicion')
	search_fields = ('titulo', 'descripcion')
	
	
	def get_queryset(self):
		queryset=super(DetallesView ,self).get_queryset()
		if self.request.GET.get('estado',None):
			if self.request.GET.get('estado',None)!="todo-mexico":
				queryset=queryset.filter(estado__estado__icontains=self.request.GET.get('estado',None))
		
		
		if self.request.GET.get("order",None)=="precio":
			queryset=queryset.order_by('precio')
		else:
			queryset=queryset.order_by('-fecha')

		return queryset
	
	def create(self, request, *args, **kwargs):
		serializer = self.get_serializer(data=request.data)
		if request.user:
			serializer.user=request.user
		serializer.is_valid(raise_exception=True)
		self.perform_create(serializer)

		data={
			'estado':Estado.objects.get(id=request.POST['estado']),
			'ciudad':Ciudad.objects.get(id=request.POST['ciudad'])

		}
		if request.user:
			data['user']=request.user
		serializer.save(**data)
 		headers = self.get_success_headers(serializer.data)
 		return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

class SubirImagen(views.APIView):
	parser_classes=(parsers.MultiPartParser,)

	def post(self, request, format=None):
		image_obj=request.data['file']
		image=Images.objects.create(image=image_obj)
		return Response({'url':image.image.url},status=200)


class ObtainAuthToken2(ObtainAuthToken):
	def post(self, request, *args, **kwargs):
		serializer = AuthTokenSerializer2(data=request.data)
		serializer.is_valid(raise_exception=True)
		user = serializer.validated_data['user']
		token, created = Token2.objects.get_or_create(user=user)
		return Response({'token': token.key})
		
obtain_auth_token2 = ObtainAuthToken2.as_view()


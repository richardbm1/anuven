from rest_framework import routers
from django.conf.urls import include, url
from api import views
from rest_framework.authtoken import views as auth


router=routers.DefaultRouter()


router.register(r'anuncios',views.DetallesView)
router.register(r'user',views.Register)


urlpatterns = [
	url(r'', include(router.urls)),
	url(r'token-auth2/', views.obtain_auth_token2),
	url(r'token-auth/', auth.obtain_auth_token),
	url(r'upload/$', views.SubirImagen.as_view()),
	#url(r'register/$', views.Register.as_view({'get':'list'})),
]
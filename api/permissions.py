from rest_framework.permissions import BasePermission

class SI(BasePermission):
	SAFE_METHODS = ('GET', 'POST','HEAD', 'OPTIONS')

	def has_permission(self,request,view):
		return True

	def check_object_permission(self, user, obj):
		return True
	def has_object_permission(self, request, view, obj):
		return True

class IsOwner(BasePermission):
	SAFE_METHODS = ('GET', 'POST','HEAD', 'OPTIONS')

	def has_permission(self,request,view):
		return True

	def check_object_permission(self, user, obj):
		return True
	def has_object_permission(self, request, view, obj):
		if request.method in self.SAFE_METHODS:
			return True
		elif request.method=='PUT' or request.method=='PATCH' or request.method=='DELETE':
			
			if obj.user:
				if obj.user == request.user:
					return True
			elif obj.userPublication:
				if obj.userPublication == request.user:
					return True
				
		return False

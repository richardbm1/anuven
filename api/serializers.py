from rest_framework import serializers
from publications.models import Publicacion,Autos_y_Camionetas, Marca_ayc, Modelo_ayc, Images, Casas, Inmuebles
from ubicacion.models import Estado, Ciudad
from django.utils.translation import ugettext_lazy as _
from accounts.models import User, UserPublication

		
class CiudadSerializer(serializers.ModelSerializer):
	class Meta:
		model = Ciudad
		fields = ('ciudad',)

class EstadoSerializer(serializers.ModelSerializer):
	class Meta:
		model = Estado
		fields = ('estado',)

class UserSerializer(serializers.ModelSerializer):
	class Meta:
		model = User
		fields = ('username','first_name','last_name','email','password','telefono','telefono2','slug',)
class UserPublicationSerializer(serializers.ModelSerializer):
	class Meta:
		model = UserPublication
		fields = ('first_name' ,'email','telefono','telefono2','slug',)

class Marca_aycSerializer(serializers.ModelSerializer):
	class Meta:
		model = Marca_ayc
		fields = ('marca',)
class Modelo_aycSerializer(serializers.ModelSerializer):
	class Meta:
		model = Modelo_ayc
		fields = ('modelo',)

class Autos_y_CamionetasSerializer(serializers.ModelSerializer):
	marca=Marca_aycSerializer()
	modelo=Modelo_aycSerializer()
	transmision = serializers.SerializerMethodField()
	combustible = serializers.SerializerMethodField()
	class Meta:
		model = Autos_y_Camionetas
		fields = ('id','marca','anio','transmision','modelo','combustible','kilometraje',)


	
	def get_transmision(self,obj):
		return obj.get_transmision_display()
	def get_combustible(self,obj):
		return obj.get_combustible_display()
class CasasSerializer(serializers.ModelSerializer):
	class Meta:
		model = Casas
		fields = ('id','metros_terreno','metros_construccion',)

class InmueblesSerializer(serializers.ModelSerializer):
	class Meta:
		model = Casas
		fields = ('id','metros_construccion',)
class ImagesSerializer(serializers.ModelSerializer):
	class Meta:
		model = Images
		fields = ('image',)

class DetallesSerializer(serializers.ModelSerializer):
	imagenes =  ImagesSerializer(source='image',read_only=True,many=True)
	particular_o_negocio= serializers.ChoiceField(choices=Publicacion.PART_O_NEG)
	categoria = serializers.ChoiceField(choices=Publicacion.CATEGORIA)
	condicion = serializers.ChoiceField(choices=Publicacion.CONDICION)
	tipo = serializers.ChoiceField(choices=Publicacion.TIPO)
	status = serializers.ChoiceField(choices=Publicacion.PUBLIC_STATUS)
	class Meta:
		depth=1
		model = Publicacion
		fields = ('id','user','userPublication','autos_y_camionetas','casas','departamentos','titulo','categoria','particular_o_negocio','imagenes','descripcion','precio','condicion','destacado','fecha','tipo','estado','ciudad','status','slug',)
		read_only_fields = ('id','imagenes',)
 	
	def create(self,validated_data):

		anuncio=Publicacion(**validated_data)
		if validated_data.get('userPublication'):
			userPub=UserPublication.objects.create(**validated_data.pop('userPublication'))
			anuncio.userPublication=UserPublication.objects.get(id=validated_data.pop('user')['id'])
		
		anuncio.save()
		return anuncio
	
class AuthTokenSerializer2(serializers.Serializer):
	username = serializers.CharField(label=_("Username"))
	password = serializers.CharField(label=_("Password"), style={'input_type': 'password'})


	def validate(self, attrs):
		username = attrs.get('username')
		password = attrs.get('password')

		if username and password:
			user = UserPublication.objects.get(id=username)
			if user.check_password(password):
				if not user.is_active:
					msg = _('User account is disabled.')
					raise serializers.ValidationError(msg)
			else:
				msg = _('Unable to log in with provided credentials.')
				raise serializers.ValidationError(msg)
		else:
			msg = _('Must include "username" and "password".')
			raise serializers.ValidationError(msg)

		attrs['user'] = user
		return attrs
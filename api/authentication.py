from rest_framework.authentication import TokenAuthentication
from api.models import Token2
from rest_framework.authtoken.models import Token
from django.utils.translation import ugettext_lazy as _

class TokenAuthentication2(TokenAuthentication):
    """
    Simple token based authentication.

    Clients should authenticate by passing the token key in the "Authorization"
    HTTP header, prepended with the string "Token ".  For example:

        Authorization: Token 401f7ac837da42b97f613d789819ff93537bee6a
    """

    model = (Token,Token2)

    def get_model(self):
        if self.model is not None:
            return self.model        
        return Token


    def authenticate_credentials(self, key):
        model, model2 = self.get_model()
        try:
            token = model.objects.select_related('user').get(key=key)
        except:
            try:
                token = model2.objects.select_related('user').get(key=key)
            except model.DoesNotExist:
                raise exceptions.AuthenticationFailed(_('Invalid token.'))

        if not token.user.is_active:
            raise exceptions.AuthenticationFailed(_('User inactive or deleted.'))

        return (token.user, token)


from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings
from publications import views
from django.contrib.auth.decorators import login_required
from ubicacion import views as view_ubi
from api import urls as api_urls

urlpatterns = [
    # Examples:
    
    url(r'^accounts/',include('accounts.urls')),
    url(r'^accounts/',include('allauth.urls')),
    url(r'^llenar/',view_ubi.LlenarDB.as_view(), name='llenar'),
    url(r'^terminos/',views.Terminos.as_view(), name='terminos'),
    url(r'^api-auth/',include(api_urls)),
    url(r'^api/',include(api_urls)),
    url(r'^$', views.HomeView.as_view(), name='home'),
    url(r'^anuncio/(?P<slug>[\w-]+)/$',views.PublicacionView.as_view(), name='anuncio'),
    url(r'^anuncio/login/(?P<slug>[\w-]+)/$',views.PublicacionLogin.as_view(), name='anunciologin'),
    url(r'^anuncio/editar/(?P<slug>[\w-]+)/$',views.PublishUpdate.as_view(), name='editar-anuncio'),
    url(r'^anuncios/',include('publications.urls')),
    url(r'^publicar/$', views.PublishForm.as_view(), name='publicar'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^marcas/$',views.BuscarMarcas.as_view(), name='buscarmarcas'),
    url(r'^modelos/$',views.BuscarModelos.as_view(), name='buscarmodelos'),
    url(r'^publicar_images/$',views.PublicarImages, name='publicar_images'),
    url(r'^image/delete/$',views.EliminarImagen.as_view(), name='eliminar_imagen'),
    url(r'^ultimasPublicaciones/',views.Ultimas_publicaciones.as_view(), name='ultimasP'),
    url(r'^publicacionesDestacadas/',views.publicaciones_destacadas.as_view(), name='PDestacadas'),
    url(r'^ciudad/$',view_ubi.BuscarCiudad.as_view(), name='buscarciudad'),
    url(r'^estado/$',view_ubi.BuscarEstado.as_view(), name='buscarestado'),
    url(r'^borrar/(?P<slug>[\w-]+)/$',views.BorrarPublicacion.as_view(), name='borrar-publicacion'),
    url(r'^send-message/$',views.SendMessage, name='send-message'),
]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns += url(r'^__debug__/', include(debug_toolbar.urls)),

from django.conf import settings
from django.views.static import serve 
if settings.DEBUG: 
    urlpatterns += [ 
        url(r'^media/(?P<path>.*)$', serve,  {'document_root': settings.MEDIA_ROOT,}), 
    ] 
urlpatterns += [ 
    url(r'^media/(?P<path>.*)$', serve,  {'document_root': settings.MEDIA_ROOT,}), 
    ] 
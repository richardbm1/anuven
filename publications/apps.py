from __future__ import unicode_literals

from django.apps import AppConfig


class PublicationsConfig(AppConfig):
    name = 'publications'
    verbose_name= 'Publicaciones'

#-*- encoding:utf-8 -*-
from django.shortcuts import render
from django.views.generic import CreateView, TemplateView, UpdateView, ListView, DetailView, DeleteView
from publications import models as publications_model
from publications import forms as publications_form
from ubicacion.models import Estado
from accounts.form import SignupPublication
from accounts.models import UserPublication
from django.core import serializers
from django.http import HttpResponse
import json
import random
from django.http import HttpResponse, JsonResponse
from rest_framework.generics import RetrieveUpdateDestroyAPIView
from rest_framework.views import APIView
#from publications.serializers import DetallesSerializer
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.renderers import TemplateHTMLRenderer, JSONRenderer, BrowsableAPIRenderer
from django.template import defaultfilters
from django.db.models import Q
from allauth.account.forms import LoginForm
from allauth.exceptions import ImmediateHttpResponse
from django.contrib.auth import authenticate, login
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from allauth.account.views import LoginView
from django.views.generic.detail import SingleObjectMixin
from django.contrib.auth.hashers import make_password
from django.core.urlresolvers import reverse
from django.template import RequestContext
from django.core.mail import send_mail
from django.core.mail import EmailMessage
from anuven import settings
from django.views.generic.base import ContextMixin
from django.db.models import Q


class Utilidades(object):

	cat_reverse=dict((v.lower().encode('utf-8'),k) for k,v in publications_model.Publicacion.CATEGORIA)
	cat=dict((k,v.lower().encode('utf-8')) for k,v in publications_model.Publicacion.CATEGORIA)
	cat_principales=['vehiculos','inmuebles','electronica','tiempo libre','personal','hogar y oficina']
	tipo_reverse=dict((v.lower().encode('utf-8'),k) for k,v in publications_model.Publicacion.TIPO)
	pot_reverse=dict((v.lower().encode('utf-8'),k) for k,v in publications_model.Publicacion.PART_O_NEG)
	def cats(self,categoria):
		cat=dict((v.lower().encode('utf-8'),[k]) for k,v in publications_model.Publicacion.CATEGORIA)
		cat['vehiculos']=['AyC','AyA','Mot','CyC','NyA']
		cat['inmuebles']=['Cas','Dep','TyL','Asi','CdC','RV']
		cat['electronica']=['Tel','CyT','ATF','CyV','OE']
		cat['tiempo libre']=['AyM','Bic','De','IM','CPC','HyC','Lib','Bol','VyV']
		cat['personal']=['RyZ','ApN','AyR','SyB','AA','Emp','SyE']
		cat['hogar y oficina']=['Ele','MyD','JyE','MyC','ApO','RyC','Tld']
		return cat[categoria]

	def cats_reverse(self,categoria):
		return [self.cat[x] for x in self.cats(categoria)]
	
	def deslug(self,slug):
		return " ".join(slug.split('-')).lower().encode('utf-8').decode('utf-8')

class Destacados(Utilidades,ContextMixin):
	def get_context_data(self,**kwargs):
		context={}
		queryset=publications_model.Publicacion.objects.all().filter(destacado=True).order_by('?')

		if self.request.path == reverse('home') or self.request.path == '/anuncios/':			
			queryset=queryset[:10]
		context.update(kwargs)		
		if 'estado' in self.kwargs.keys():
			if self.deslug(self.kwargs['estado'])!='todo mexico':
				queryset=queryset.filter(estado__estado__icontains=self.deslug(self.kwargs['estado']))
		if 'categoria' in self.kwargs.keys():
			if self.deslug(self.kwargs['categoria'])=='vehiculos':
				queryset=queryset.filter(Q(categoria__icontains="AyC") | Q(categoria__icontains="AyA") | Q(categoria__icontains="Mot") | Q(categoria__icontains="CyC") | Q(categoria__icontains="NyA"))
			elif self.deslug(self.kwargs['categoria'])=='inmuebles':
				queryset=queryset.filter(Q(categoria__icontains="Cas") | Q(categoria__icontains="Dep") | Q(categoria__icontains="TyL") | Q(categoria__icontains="Asi") | Q(categoria__icontains="CdC") | Q(categoria__icontains="RV"))
			elif self.deslug(self.kwargs['categoria'])=='electronica'.decode('utf-8'):
				queryset=queryset.filter(Q(categoria__icontains="Tel") | Q(categoria__icontains="CyT") | Q(categoria__icontains="ATF") | Q(categoria__icontains="CyV") | Q(categoria__icontains="OE"))
			elif self.deslug(self.kwargs['categoria'])=='tiempo libre':
				queryset=queryset.filter(Q(categoria__icontains="Bic") | Q(categoria__icontains="De") | Q(categoria__icontains="IM") | Q(categoria__icontains="CPC") | Q(categoria__icontains="HyC") | Q(categoria__icontains="Lib") | Q(categoria__icontains="Bol") | Q(categoria__icontains="VyV"))
			elif self.deslug(self.kwargs['categoria'])=='personal':
				queryset=queryset.filter(Q(categoria__icontains="RyZ") | Q(categoria__icontains="ApN") | Q(categoria__icontains="AyR") | Q(categoria__icontains="SyB") | Q(categoria__icontains="AA") | Q(categoria__icontains="Emp") | Q(categoria__icontains="SyE"))
			elif self.deslug(self.kwargs['categoria'])=='hogar y oficina':
				queryset=queryset.filter(Q(categoria__icontains="Ele") | Q(categoria__icontains="MyD") | Q(categoria__icontains="JyE") | Q(categoria__icontains="MyC") | Q(categoria__icontains="ApO") | Q(categoria__icontains="RyC") | Q(categoria__icontains="Tld"))

			else:
				cat_deslug=self.deslug(self.kwargs['categoria'])
				if cat_deslug in self.cat_reverse.keys():
					queryset=queryset.filter(categoria__icontains=self.cat_reverse[cat_deslug])

			if self.request.GET.get("Marca",None):
				queryset=queryset.filter(autos_y_camionetas__marca__marca__icontains=self.request.GET.get("Marca",None))
		queryset=queryset[:13]

		context['destacados']=queryset
		return super(Destacados,self).get_context_data(**context)
 

class AnunciosListView(Destacados,ListView):
	template_name="publications/listado.html"
	model=publications_model.Publicacion
	paginate_by=10

	def len_cats(self,categoria):
		return eval('+'.join([str(len(self.get_queryset().filter(categoria__icontains=x))) for x in self.cats(categoria)]))
	

	def get_context_data(self,*args,**kwargs):
		context=super(AnunciosListView,self).get_context_data(**kwargs)
		
		if 'estado' in self.kwargs.keys():
			context['title']="clasificados de %s"%self.deslug(self.kwargs['estado'])
			context['estado']=self.deslug(self.kwargs['estado'])
			context['estadoslug']=self.kwargs['estado'].encode('utf-8')
			if self.deslug(self.kwargs['estado'])=="todo mexico":
				context['estados']=[[x.estado.replace(" ","-"),"%s (%d)"%(x,len(self.get_queryset().filter(estado__estado=x)))] for x in Estado.objects.all()]
		
		else:
			context['estado']="Todo Mexico"
			context['estadoslug']="Todo-Mexico"
			context['title']="clasificados de Todo Mexico"
		if 'categoria' in self.kwargs.keys() and self.kwargs['categoria']!="todas-las-categorias":

			context['title']="%s en %s"%(self.deslug(self.kwargs['categoria']),self.deslug(self.kwargs['estado']))
			
			context['cat']=self.deslug(self.kwargs['categoria'])
			context['catslug']=self.kwargs['categoria']
			if self.deslug(self.kwargs['categoria'])=='vehiculos':
				context['categorias']=[[defaultfilters.slugify(x),"%s (%d)"%(x,self.len_cats(x))] for x in self.cats_reverse('vehiculos')]
			elif self.deslug(self.kwargs['categoria'])=='inmuebles':
				context['categorias']=[[defaultfilters.slugify(x),"%s (%d)"%(x,self.len_cats(x))] for x in self.cats_reverse('inmuebles')]
			elif self.deslug(self.kwargs['categoria'])=='electronica'.decode('utf-8'):
				context['categorias']=[[defaultfilters.slugify(x),"%s (%d)"%(x,self.len_cats(x))] for x in self.cats_reverse('electronica'.decode('utf-8'))]
			elif self.deslug(self.kwargs['categoria'])=='tiempo libre':
				context['categorias']=[[defaultfilters.slugify(x),"%s (%d)"%(x,self.len_cats(x))] for x in self.cats_reverse('tiempo libre')]
			elif self.deslug(self.kwargs['categoria'])=='personal':
				context['categorias']=[[defaultfilters.slugify(x),"%s (%d)"%(x,self.len_cats(x))] for x in self.cats_reverse('personal')]
			elif self.deslug(self.kwargs['categoria'])=='hogar y oficina':
				context['categorias']=[[defaultfilters.slugify(x),"%s (%d)"%(x,self.len_cats(x))] for x in self.cats_reverse('hogar y oficina')]

			else:
				if self.deslug(self.kwargs['categoria'])=='autos y camionetas' and self.request.GET.get("Marca",None)==None:
					context['subcat']='Marca'
					context['valsubcat']=[[defaultfilters.slugify(x),"%s (%d)"%(x.marca,len(self.get_queryset().filter(autos_y_camionetas__marca__marca=x.marca)))]  for x  in publications_model.Marca_ayc.objects.all()]
				
			
		else:
			context['cat']= "todas las categorias"
			context['catslug']= "todas-las-categorias"
			context['categorias']=[[defaultfilters.slugify(x),"%s (%d)"%(x,self.len_cats(x))] for x in self.cat_principales]
		context['title']="Resultado"
		if "titulo" in self.kwargs.keys():
			context['titulo']=self.deslug(self.kwargs['titulo'])
			context['tituloslug']=self.kwargs['titulo']
			context['title']="%s en %s"%(self.deslug(self.kwargs['titulo']),self.deslug(self.kwargs['estado']))

		if not self.request.GET.get("Tipo",None):
			context['tipocompra']=len(self.get_queryset().filter(tipo="co"))
			context['tipoventa']=len(self.get_queryset().filter(tipo="ve"))
			context['tiposervicio']=len(self.get_queryset().filter(tipo="se"))
		if not self.request.GET.get("particular_o_negocio",None):
			context['pot_particular']=len(self.get_queryset().filter(particular_o_negocio="PAR"))
			context['pot_negocio']=len(self.get_queryset().filter(particular_o_negocio="NEG"))
			
		return context

	def get_queryset(self):
		queryset=super(AnunciosListView,self).get_queryset()
		if 'estado' in self.kwargs.keys():
			if self.kwargs['estado'].lower()!="todo-mexico":
				queryset=queryset.filter(estado__estado__icontains=self.deslug(self.kwargs['estado']))
		

		if 'categoria' in self.kwargs.keys():
			if self.deslug(self.kwargs['categoria'])=='vehiculos':
				queryset=queryset.filter(Q(categoria__icontains="AyC") | Q(categoria__icontains="AyA") | Q(categoria__icontains="Mot") | Q(categoria__icontains="CyC") | Q(categoria__icontains="NyA"))
			elif self.deslug(self.kwargs['categoria'])=='inmuebles':
				queryset=queryset.filter(Q(categoria__icontains="Cas") | Q(categoria__icontains="Dep") | Q(categoria__icontains="TyL") | Q(categoria__icontains="Asi") | Q(categoria__icontains="CdC") | Q(categoria__icontains="RV"))
			elif self.deslug(self.kwargs['categoria'])=='electronica'.decode('utf-8'):
				queryset=queryset.filter(Q(categoria__icontains="Tel") | Q(categoria__icontains="CyT") | Q(categoria__icontains="ATF") | Q(categoria__icontains="CyV") | Q(categoria__icontains="OE"))
			elif self.deslug(self.kwargs['categoria'])=='tiempo libre':
				queryset=queryset.filter(Q(categoria__icontains="Bic") | Q(categoria__icontains="De") | Q(categoria__icontains="IM") | Q(categoria__icontains="CPC") | Q(categoria__icontains="HyC") | Q(categoria__icontains="Lib") | Q(categoria__icontains="Bol") | Q(categoria__icontains="VyV"))
			elif self.deslug(self.kwargs['categoria'])=='personal':
				queryset=queryset.filter(Q(categoria__icontains="RyZ") | Q(categoria__icontains="ApN") | Q(categoria__icontains="AyR") | Q(categoria__icontains="SyB") | Q(categoria__icontains="AA") | Q(categoria__icontains="Emp") | Q(categoria__icontains="SyE"))
			elif self.deslug(self.kwargs['categoria'])=='hogar y oficina':
				queryset=queryset.filter(Q(categoria__icontains="Ele") | Q(categoria__icontains="MyD") | Q(categoria__icontains="JyE") | Q(categoria__icontains="MyC") | Q(categoria__icontains="ApO") | Q(categoria__icontains="RyC") | Q(categoria__icontains="Tld"))

			else:
				cat_deslug=self.deslug(self.kwargs['categoria'])
				if cat_deslug in self.cat_reverse.keys():
					queryset=queryset.filter(categoria__icontains=self.cat_reverse[cat_deslug])

			if self.request.GET.get("Marca",None):
				queryset=queryset.filter(autos_y_camionetas__marca__marca__icontains=self.request.GET.get("Marca",None))

		if 'titulo' in self.kwargs.keys():
			queryset=queryset.filter(titulo__icontains=self.deslug(self.kwargs['titulo']))

		if self.request.GET.get("Tipo",None):
			queryset=queryset.filter(tipo__icontains=self.tipo_reverse[self.deslug(self.request.GET.get("Tipo",None))])
		if self.request.GET.get("particular_o_negocio",None):
			queryset=queryset.filter(particular_o_negocio__icontains=self.pot_reverse[self.deslug(self.request.GET.get("particular_o_negocio",None))])
		

		return queryset.order_by('-fecha')



class Terminos(TemplateView):

	def get_context_data(self,**kwargs):
		context=super(Terminos,self).get_context_data(**kwargs)
		self.template_name="publications/terminos.html"
		context['title']="Terminos y condiciones"
		return context

def handler404(request):
    response = render_to_response('404.html', {},
                                  context_instance=RequestContext(request))
    response.status_code = 404
    return response


def handler500(request):
    response = render_to_response('500.html', {},
                                  context_instance=RequestContext(request))
    response.status_code = 500
    return response
    
class Templates(TemplateView):

	def get_context_data(self,**kwargs):
		context=super(Templates,self).get_context_data(**kwargs)
		self.template_name="publications/"+self.kwargs["slug"]+".html"
		context['title']=self.kwargs["slug"]
		return context
class TemplatesView(TemplateView):
	def get_context_data(self, **kwargs):
		context = super(TemplatesView, self).get_context_data(**kwargs)
		direc=settings.BASE_DIR+"/templates/how/" + context["template"] + ".html"
		
		try:
			open(direc.encode('utf-8'))
			self.template_name = "how/" + context["template"] + ".html"
			context['title']="Hotel a la venta"
		except:
			self.template_name = "404.html"
			context['title']="Pagina no encontrada"
		context['foot_last']=model_h.Hotel.objects.all().filter(status="1").order_by('-date')[:3]
		context['contact']=model_h.DataContact.objects.get(slug='data')

		return context

class HomeView(Destacados,TemplateView):

	def get_context_data(self,**kwargs):
		context=super(HomeView,self).get_context_data(**kwargs)
		self.template_name="publications/home.html"
		context['title']='inicio'
		context['publicaciones']=publications_model.Publicacion.objects.all().filter().order_by('-fecha')[:10]
		#context['destacados']=publications_model.Publicacion.objects.all().filter(destacado=True).order_by('?')[:10]
		return context

class PublishView(TemplateView):

	def get_context_data(self,**kwargs):
		context=super(PublishView,self).get_context_data(**kwargs)
		self.template_name="publications/formulario.html"
		context['title']='Publicar'
		return context


class PublicacionLogin(LoginView,SingleObjectMixin):
	template_name="publications/details.html"
	model=publications_model.Publicacion

	def get_context_data(self,**kwargs):
		context=super(PublicacionView,self).get_context_data(**kwargs)
		context['title']=self.object.titulo
		return context

class PublicacionView(Destacados,DetailView):

	template_name="publications/details.html"
	model=publications_model.Publicacion
	password=""

	def get_context_data(self,**kwargs):
		context=super(PublicacionView,self).get_context_data(**kwargs)
		context['title']=self.object.titulo
		return context


	def post(self, request, *args, **kwargs):
		self.object=self.get_object()
		if make_password(password=self.request.POST["password"], salt=None,hasher='unsalted_md5')==self.object.userPublication.password:
			self.request.session['userPub']=self.object.userPublication.id
			return HttpResponseRedirect("/anuncio/editar/"+self.object.slug)
		else:
			context=self.get_context_data()
			context['password']="contraseña incorrecta"
			return self.render_to_response(context)
		"""
		if self.request.POST.get("email",None) and self.request.POST.get("password",None):
			login=LoginForm(self.request.POST)
			if login.is_valid():
				try:
					response = login.login(self.request,redirect_url="/anuncio/editar/"+self.object.slug)
				except ImmediateHttpResponse as e:
					response = e.response
				return HttpResponseRedirect("/anuncio/editar/"+self.object.slug)

				
			else:
				return  HttpResponseRedirect("/anuncio/"+self.object.slug)
		"""
	




"""
class HomeView(APIView):
	
	renderer_classes=[]
	#renderer_classes+=[BrowsableAPIRenderer]
	renderer_classes+=[JSONRenderer,TemplateRenderer]
	queryset=publications_model.Publicacion.objects.all()
	serializer_class = DetallesSerializer(many=True)
	template_name="publications/home.html"
	def get(self, request):
		return Response(self.serializer_class(self.queryset))

	def get_renderer_context(self):
		return {
				'view': self,
				'args': getattr(self, 'args', ()),
				'kwargs': getattr(self, 'kwargs', {}),
				'request': getattr(self, 'request', None),
				'publicaciones': DetallesSerializer(publications_model.Publicacion.objects.all(),many=True,read_only=True).data,
				}
"""


	


class Ultimas_publicaciones(TemplateView):
	def get(self, request, *args, **kwargs):
		
		if self.request.GET.get('cantidad',None):

			ultimasP=publications_model.Publicacion.objects.all().order_by('-id')[:self.request.GET.get('cantidad',None)]
			
			data=[]
			
			for public in ultimasP:
				print public.imagenes.all()
				image=""
				try:
					image=public.imagenes.all()[0]
					image="/media/"+str(image.image)
				except:
					image="/static/image/avatar_default.png"
				jsons={'titulo':str(public.titulo),'precio':str(public.precio),
							 'imagen_principal':str(image),'estado':public.estado.estado,
							 'slug':public.slug}
				data.append(jsons)
			
			"""
		
			for public in ultimasP:
				try:
					public.image=public.imagenes.all()
				except:
					public.image=""

			"""
			#data = serializers.serialize('json',ultimasP,fields=('titulo','slug','image','precio','estado'))
			print data
		return HttpResponse(json.dumps(data) , content_type="application/json")


class publicaciones_destacadas(TemplateView):
	def get(self, request, *args, **kwargs):
		
		if self.request.GET.get('cantidad',None):

			destacado=publications_model.Publicacion.objects.all().filter(destacado=True).order_by('?')[:self.request.GET.get('cantidad',None)]
			data=[]
			
			for public in destacado:
				print public.imagenes.all()
				image=""
				try:
					image=public.imagenes.all()[0]
					image="/media/"+str(image.image)
				except:
					image="/static/image/avatar_default.png"
				jsons={'titulo':str(public.titulo),'precio':str(public.precio),
							 'imagen_principal':str(image),'estado':public.estado.estado,
							 'slug':public.slug,'descripcion':public.descripcion[:50]+'...'}
				data.append(jsons)
			
			"""
		
			for public in ultimasP:
				try:
					public.image=public.imagenes.all()
				except:
					public.image=""

			"""
			#data = serializers.serialize('json',ultimasP,fields=('titulo','slug','image','precio','estado'))
			print data
		return HttpResponse(json.dumps(data) , content_type="application/json")

class PublishForm(CreateView):
	template_name="publications/formulario.html"
	model=publications_model.Publicacion
	form_class=publications_form.PublicationForm

	def get_context_data(self, **kwargs):
		context = super(PublishForm, self).get_context_data(**kwargs)
		context['title']="Publicar"
		context['casas']=publications_form.CasasForm
		context['inmuebles']=publications_form.InmueblesForm
		context['autos']=publications_form.Autos_y_CamionetasForm
		context['users']=SignupPublication
		context['update']=False
		return context

	def get_success_url(self):
		return reverse('anuncio', kwargs={'slug': self.object.slug})

	#def post(self, request, *args, **kwargs):
	#	print request.POST
	#	data={}
	#	return HttpResponse(data , content_type="application/json")

	def form_valid(self,form):
		userform=SignupPublication(self.request.POST)

		if userform.is_valid():


			user=userform.save(commit=True)
			user.password=make_password(password=user.password, salt=None,hasher='unsalted_md5')
			user.save()
		else:
			return super(PublishForm,self).form_invalid(userform)
		cat=self.request.POST.get("categoria",None)
		if cat:
			publication=form.save(commit=False)
			if cat == "AyC":
				
				aycform=publications_form.Autos_y_CamionetasForm(self.request.POST)
				if aycform.is_valid():
					ayc=aycform.save(commit=True)
					ayc.save()
					publication.autos_y_camionetas=ayc	
				else:
					return super(PublishForm,self).form_invalid(aycform)
			elif cat == "Cas":
				casform=publications_form.CasasForm(self.request.POST)
				if casform.is_valid():
					cas=casform.save(commit=True)
					cas.save()
					publication.casas=cas

			
		publication.userPublication=user		
		publication.save()
		
		for img in self.request.FILES.getlist('file-es[]',None):
			image = publications_model.Images(image=img)
			image.save()
			publication.imagenes.add(image)	

			
			
				
		return super(PublishForm,self).form_valid(form)


		#if(len(self.request.FILES.getlist('images')) > 5):
		#	messages.success(self.request, "No puede contener mas de 5 imagenes.", extra_tags='publicationimage')
		#	return super(PublishForm,self).form_invalid(form)
class PublishUpdate(UpdateView):
	template_name="publications/formulario.html"
	model=publications_model.Publicacion
	form_class=publications_form.PublicationForm

	def dispatch(self, request, *args, **kwargs):
		self.object=self.get_object()
		try:
			session=self.request.session['userPub']
			if session!=self.object.userPublication.id:
				return HttpResponseRedirect("/anuncio/"+self.object.slug)
		
		except:
			return HttpResponseRedirect("/anuncio/"+self.object.slug)

		if request.method.lower() in self.http_method_names:
			handler = getattr(self, request.method.lower(), self.http_method_not_allowed)
		else:
			handler = self.http_method_not_allowed
		return handler(request, *args, **kwargs)

	def get_context_data(self, **kwargs):
		
		context = super(PublishUpdate, self).get_context_data(**kwargs)
		context['title']="Publicar"
		context['update']=True

		context['categoria']=self.object.get_categoria_display()
		context['id_categoria']=self.object.categoria
		if self.object.autos_y_camionetas:
			context['autos']=self.object.autos_y_camionetas
			print self.object.get_categoria_display()
			
		if self.object.user:
			pass
		if self.object.userPublication:

			context['users']=self.object.userPublication
		#context['casas']=publications_form.CasasForm
		#context['inmuebles']=publications_form.InmueblesForm
		try:
			session=self.request.session['userPub']
			if session!=self.object.userPublication.id:
				return HttpResponseRedirect("/anuncio/"+self.object.slug)
		
		except:
			return HttpResponseRedirect("/anuncio/"+self.object.slug)

		return context

	def get_success_url(self):
		return reverse('anuncio', kwargs={'slug': self.object.slug})

	#def post(self, request, *args, **kwargs):
	#	print request.POST
	#	data={}
	#	return HttpResponse(data , content_type="application/json")

	def form_valid(self,form):
		userform=SignupPublication(self.request.POST)

		if userform.is_valid():
			UserPublication.objects.filter(id=self.object.userPublication.id).update(**userform.cleaned_data)
			if not self.request.POST['password']==self.object.userPublication.password:				
				UserPublication.objects.filter(id=self.object.userPublication.id).update(password=make_password(password=self.request.POST['password'], salt=None,hasher='unsalted_md5'))
		else:
			return super(PublishForm,self).form_invalid(userform)
		cat=self.request.POST.get("categoria",None)

		if cat:
			publication=form.save(commit=False)
			if cat == "AyC":
				
				aycform=publications_form.Autos_y_CamionetasForm(self.request.POST)
				if aycform.is_valid():
					ayc=aycform.save(commit=True)
					ayc.save()
					publication.autos_y_camionetas=ayc	
				else:
					return super(PublishForm,self).form_invalid(aycform)
			elif cat == "Cas":
				casform=publications_form.CasasForm(self.request.POST)
				if casform.is_valid():
					cas=casform.save(commit=True)
					cas.save()
					publication.casas=cas

			
		
		print self.request.FILES.getlist('file-es[]',None)
		for img in self.request.FILES.getlist('file-es[]',None):
			image = publications_model.Images(image=img)
			image.save()
			publication.imagenes.add(image)	

			
			
				
		return super(PublishUpdate,self).form_valid(form)


class BorrarPublicacion(DeleteView):
	model=publications_model.Publicacion
	def dispatch(self, request, *args, **kwargs):
		self.object=self.get_object()
		try:
			session=self.request.session['userPub']
			if session!=self.object.userPublication.id:
				return HttpResponseRedirect("/anuncio/"+self.object.slug)
		
		except:
			return HttpResponseRedirect("/anuncio/"+self.object.slug)

		if request.method.lower() in self.http_method_names:
			handler = getattr(self, request.method.lower(), self.http_method_not_allowed)
		else:
			handler = self.http_method_not_allowed
		return handler(request, *args, **kwargs)
	def get_success_url(self):
		return reverse('home')

class BuscarMarcas(TemplateView):
	def get(self, request, *args, **kwargs):
		marca = publications_model.Marca_ayc.objects.all()
		data = serializers.serialize('json',marca,
			fields=('id','marca'))
		return HttpResponse(data , content_type="application/json")

class BuscarModelos(TemplateView):
	def get(self, request, *args, **kwargs):
		id_marca = request.GET['id']
		modelo = publications_model.Modelo_ayc.objects.filter(marca__id=id_marca)
		data = serializers.serialize('json',modelo,
			fields=('id','modelo'))
		return HttpResponse(data , content_type="application/json")

def PublicarImages(request):
	if request.POST:
		pub=request.POST.get('publicacion',None)
		publication=publications_model.Publicacion.objects.get(id=pub)
		for img in request.FILES.getlist('file-es[]',None):
			image = publications_model.Images(image=img)
			image.save()
			publication.imagenes.add(image)	
				
		data={'success':True}
		return JsonResponse(data)

def SendMessage(request):
	if request.POST:
		nombre=request.POST.get('nombre',None)
		telefono=request.POST.get('telefono',"")
		email=request.POST.get('email',None)
		comentario=request.POST.get('comentario',None)
		email_vendedor=request.POST.get('email_vendedor',None)
		titulo_pub=request.POST.get('titulo',None)

		titulo="["+titulo_pub+"]ha recibido un mensaje"
		contenido="Nombre: "+nombre+"\nTelefono: "
		contenido+=telefono+"\nEmail: "
		contenido+=email+"\n\nMensaje: "
		contenido+=comentario 
		email=EmailMessage(
				titulo,
				contenido,
				settings.EMAIL_HOST_USER,
				[email_vendedor],
				reply_to=[email],
				headers={'Message-ID': 'foo'}

			)
		email.send(fail_silently=False)

		#send_mail(titulo, contenido,settings.EMAIL_HOST_USER,[email_vendedor], fail_silently=False)
				
		data={'success':True}
		return JsonResponse(data)

class EliminarImagen(TemplateView):
	template_name = "hola.html"
	def post(self, request, *args, **kwargs):
		image=request.POST['key']
		publications_model.Images.objects.get(id=image).delete()
		data={'success':True}
		return JsonResponse(data)
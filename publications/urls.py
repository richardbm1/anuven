from django.conf.urls import include, url
from django.contrib import admin
from publications import views
from django.contrib.auth.decorators import login_required





urlpatterns = [
	
    url(r'^(?P<estado>[\w-]+)/(?P<categoria>[\w-]+)/(?P<titulo>[\w-]+)/$',views.AnunciosListView.as_view(),name='listado'),
	url(r'^(?P<estado>[\w-]+)/(?P<categoria>[\w-]+)/$',views.AnunciosListView.as_view(),name='listado'),
    url(r'^(?P<estado>[\w-]+)/$',views.AnunciosListView.as_view(),name='listado'),   
    url(r'^$',views.AnunciosListView.as_view(),name='listado'),
    url(r'^publicar/',views.PublishForm.as_view(), name='publicar'),

    #url(r'^editar/(?P<slug>[\w-]+)/$',views.PublishUpdate.as_view(), name='editar'),  
   
    #url(r'^(?P<slug>[\w-]+)/$',views.Templates.as_view(), name='template'),
    
    
    
]

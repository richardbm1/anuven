# -*- encoding:utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from sorl.thumbnail import ImageField
from accounts.models import User, UserPublication
from django.template import defaultfilters
from django.core.urlresolvers import reverse
from ubicacion.models import Estado, Ciudad
from anuven import settings
# Create your models here.


class Images(models.Model):
	image=ImageField(upload_to='banners')

	def get_images(self):
		if (self.image):
			return '%s%s' % (settings.MEDIA_URL,self.image)
		else:
			return '%s%s' % (settings.STATIC_URL,"image/avatar_default.png")
	class Meta:
		verbose_name = 'Imagen'
		verbose_name_plural = 'Imagenes'
	



class Casas(models.Model):
	metros_construccion = models.IntegerField()
	metros_terreno = models.IntegerField()

class Inmuebles(models.Model):
	metros_construccion = models.IntegerField()

ANIO=[(str(x),x) for x in range(1920,2017)]

class Marca_ayc(models.Model):
	marca=models.CharField(max_length=50)
	def __unicode__(self):
		return self.marca
	class Meta:
		ordering=['marca']
		verbose_name = 'Marca AyC'
		verbose_name_plural = 'Marca AyC'

class Modelo_ayc(models.Model):
	modelo=models.CharField(max_length=50)
	marca=models.ForeignKey(Marca_ayc)

	def __unicode__(self):
		return self.modelo
	class Meta:
		ordering=['modelo']
		verbose_name = 'Modelo AyC'
		verbose_name_plural = 'Modelo AyC'

class Autos_y_Camionetas(models.Model):
	TRANSMISION=(
		('','Seleccione'),
		('0','Manual'),
		('1','Automatica'),
	)
	MARCA_AUTOS=(
		('','Seleccione'),
		('0','Chevrolet'),
	)
	MODELO=(
		('','Seleccione'),
		('0','Camaro'),
	)
	COMBUSTIBLE=(
		('','Seleccione'),
		('0','Gasolina'),
		('1','Diesel'),
		('2','Gas'),
		('3','Electrico'),
		('4','Híbrido'),
	)
	marca = models.ForeignKey(Marca_ayc)
	anio = models.CharField(max_length=4,choices=ANIO)
	transmision = models.CharField(max_length=3,choices=TRANSMISION)
	modelo = models.ForeignKey(Modelo_ayc)
	combustible = models.CharField(max_length=3,choices=COMBUSTIBLE)
	kilometraje = models.IntegerField()

	def __unicode__(self):
		return self.modelo.modelo


class Publicacion(models.Model):
	CATEGORIA=(
		('','---Vehículos---'),
		('AyC','Autos y camionetas'),
		('AyA','Autopartes y Accesorios'),
		('Mot','Motociletas'),
		('CyC','Camiones y Comerciales'),
		('NyA','Náutica y Aviación'),
		('Cas','Casas'),
		('Dep','Departamentos'),
		('TyL','Terrenos y Lotes'),
		('Asi','Asistencias y Cuartos'),
		('CdC','Casas de Campo'),
		('Tra','Traspasos'),
		('RV','Rentas Vacacionales'),
		('Tel','Telefonos'),
		('CyT','Computadoras y Tablets'),
		('ATF','Audio, TV, Fotos y Videos'),
		('CyV','Consolas y Videojuegos'),
		('OE','Otros Electrónicos'),
		('AyM','Animales y Mascotas'),
		('Bic','Bicicleta'),
		('De','Deportes'),
		('IM','Instrumentos Musicales'),
		('CPC','Caza, Pesca y Camping'),
		('HyC','Hobby y Coleccionables'),
		('Lib','Libros'),
		('Bol','Boletos'),
		('VyV','Viajes y Vacaciones'),
		('RyZ','Ropa y Zapatos'),
		('ApN','Articulos para Niños'),
		('AyR','Accesorio y relojes'),
		('SyB','Salud y Belleza'),
		('AA','Amor y Amistad'),
		('Emp','Empleo'),
		('SyE','Servcios y Eventos'),
		('Ele','Electrodomésticos'),
		('MyD','Mueble y Decoracion'),
		('JyE','Jardin y Exterior'),
		('MyC','Maquinaria y Construcción'),
		('ApO','Articulos para la Oficina'),
		('RyC','Restaurantes y Comidas'),
		('Tld','Todo lo demás'),
	)

	PUBLIC_STATUS=(
		('0','Pendiente'),
		('1','Aprobado'),
		('2','Rechazado'),
		('3','Borrador'),
	)

	PART_O_NEG=(
		('PAR','Particular'),
		('NEG','Negocio'),
	)

	CONDICION=(
		('NU','Nuevo'),
		('US','Usado'),
	)
	TIPO=(
		('ve','Venta'),
		('co','Compra'),
		('se','Servicio'),
	)

	user=models.ForeignKey(User,null=True,blank=True)
	userPublication=models.ForeignKey(UserPublication,null=True,blank=True)
	#campos por categoria
	autos_y_camionetas = models.OneToOneField(Autos_y_Camionetas,null=True,  blank=True, related_name='ayc')
	casas = models.OneToOneField(Casas, null=True, blank=True)
	departamentos = models.OneToOneField(Inmuebles,null=True, blank=True)

	#campos comunes
	titulo=models.CharField(max_length=100)
	categoria=models.CharField(choices=CATEGORIA, max_length=3)
	particular_o_negocio=models.CharField(choices=PART_O_NEG,max_length=3)
	descripcion=models.TextField(blank=True)
	precio=models.DecimalField(max_digits=15, decimal_places=2)
	imagenes=models.ManyToManyField(Images, blank=True)
	condicion = models.CharField(choices=CONDICION,max_length=2)
	destacado = models.BooleanField()
	fecha = models.DateTimeField(auto_now_add=True)
	date = models.DateTimeField(auto_now_add=True)
	tipo = models.CharField(max_length=3,choices=TIPO)
	estado=models.ForeignKey(Estado,null=True,blank=True)
	ciudad=models.ForeignKey(Ciudad,null=True,blank=True)

	#campos para otros procesos
	status=models.CharField(choices=PUBLIC_STATUS,max_length=20, default="3")
	slug = models.SlugField(max_length=100,blank=True,null=True)

	def __unicode__(self):
		return self.titulo

	class Meta:
		verbose_name = 'Publicacion'
		verbose_name_plural = 'Publicaciones'

	def get_precio(self):
		return str(self.precio)
	def get_imagen_thumb(self):
		if (self.imagenes.all()):
			print self.imagenes.all()[0].image
			
			return '%s' % (self.imagenes.all()[0].image)
		else:
			return None
	def get_imagen(self):
		if (self.imagenes.all()):
			
			return '%s%s' % (settings.MEDIA_URL,self.imagenes.all()[0].image)
		else:
			return '%s%s' % (settings.STATIC_URL,"image/avatar_default.png")

	def get_absolute_url(self):
		return reverse('property-detail', kwargs={'slug': self.slug})

	def save(self, *args, **kwargs):
		super(Publicacion,self).save(*args,**kwargs)
		self.slug = defaultfilters.slugify(str(self.id)+" "+self.titulo)
		super(Publicacion,self).save(*args,**kwargs)

from django.contrib import admin
from models import Publicacion, Images, Marca_ayc, Modelo_ayc, Autos_y_Camionetas
# Register your models here.

admin.site.register(Publicacion)
admin.site.register(Images)
admin.site.register(Modelo_ayc)
admin.site.register(Marca_ayc)
admin.site.register(Autos_y_Camionetas)

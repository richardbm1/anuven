from random import choice
from publications.models import Publicacion
from django.core.urlresolvers import reverse

def destacados(request):
	destacado={}
	if request.path == reverse('home'):		
		des=Publicacion.objects.filter(destacado=True)
		if des:
			destacado['destacado']=choice(des)

	elif request.path == '/anuncios/':		
		des=Publicacion.objects.filter(destacado=True)
		if des:
			destacado['destacado']=choice(des)
		
	return destacado
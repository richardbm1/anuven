from django import forms
from models import Publicacion, Inmuebles, Casas, Autos_y_Camionetas, Images

class ImagesForm(forms.ModelForm):
	
	images=forms.FileInput()

	def clean_images(self):
		if len(self.cleaned_data['images'])>5:
			raise dforms.ValidationError(_("Limite de imagenes excedido("+str(len(self.request.POST.getlist('images',0)))+" de 5)"))
		return self.cleaned_data

	def save(self,request):
		for img in self.cleaned_data['images']:
			image = models.Images(image=img)
			image.save()
			hotel.images.add(image)
		return super(ImagesForm,self).save(request)

class PublicationForm(forms.ModelForm):
	
	class Meta:
		model=Publicacion
		fields = ['titulo','categoria','particular_o_negocio','descripcion','tipo','precio','condicion','destacado','estado','ciudad']
		widgets = {
			#'precio':forms.TextInput(attrs={'required':True}),
			'name':forms.TextInput(attrs={'id':'inputPropertyFirstName'}),
			'avatar':forms.FileInput(attrs={'id':"input-2",'class':"file",'multiple':"false",'data-show-upload':"true",'data-show-caption':"true"}),
		}

class CasasForm(forms.ModelForm):
	class Meta:
		model=Casas
		fields = ['metros_construccion','metros_terreno']

class InmueblesForm(forms.ModelForm):
	class Meta:
		model=Inmuebles
		fields = ['metros_construccion']

class Autos_y_CamionetasForm(forms.ModelForm):
	class Meta:
		model=Autos_y_Camionetas
		fields = ['marca','anio','transmision','modelo','combustible','kilometraje']
	
	
class ContactForm(forms.Form):
	id_property=forms.IntegerField()
	name=forms.CharField(max_length=30)
	email=forms.EmailField()
	message=forms.CharField(widget=forms.Textarea)

